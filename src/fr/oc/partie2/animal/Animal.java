package fr.oc.partie2.animal;

abstract class Animal {

	protected int poids;
	protected String couleur;

	protected void manger() {
		System.out.println("Je mange de la viande.");
	}

	protected void boire() {
		System.out.println("Je bois de l'eau.");
	}

	abstract void deplacer();

	abstract void crier();

	public String toString() {
		String str = "Je suis un objet de la classe " + this.getClass() + ". Je suis de couleur " + this.couleur
				+ " et pèse " + this.poids +" kg.";
		return str;
	}

}
