package fr.oc.partie2.animal;

public class Tigre extends Felin {

	public Tigre() {
		
	}

	public Tigre(String couleur, int poids) {
		this.couleur = couleur;
		this.poids = poids;
	}

	@Override
	void crier() {
		System.out.println("Je grogne très fort.");
	}

	@Override
	void deplacer() {
		// TODO Auto-generated method stub
		
	}

}
