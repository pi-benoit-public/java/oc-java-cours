package fr.oc.partie2.animal;

public interface IChienSpecificite {
	public void faireCalin();
	public void faireLechouille();
	public void faireLeBeau();
}
