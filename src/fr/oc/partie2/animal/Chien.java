package fr.oc.partie2.animal;

public class Chien extends Canin implements IChienSpecificite {

	public Chien() {

	}

	public Chien(String couleur, int poids) {
		this.couleur = couleur;
		this.poids = poids;
	}

	@Override
	void crier() {
		System.out.println("J'aboie sans raison.");
	}

	@Override
	public void faireCalin() {
		System.out.println("Je te fais un gros câlin.");
	}

	@Override
	public void faireLechouille() {
		System.out.println("Je te lèche partout.");
	}

	@Override
	public void faireLeBeau() {
		System.out.println("Je lève la patte.");
	}

}
