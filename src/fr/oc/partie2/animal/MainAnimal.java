package fr.oc.partie2.animal;

public class MainAnimal {

	public static void main(String[] args) {
		Loup l = new Loup("gris bleuté", 25);
		l.boire();
		l.manger();
		l.deplacer();
		l.crier();
		System.out.println(l.toString());
		System.out.println();

		Animal loup = new Loup("gris blanc", 18);
		loup.boire();
		loup.manger();
		loup.deplacer();
		loup.crier();
		System.out.println(loup.toString());
		System.out.println();

		Animal chat = new Chat("tigré", 3);
		chat.boire();
		chat.manger();
		chat.deplacer();
		chat.crier();
		System.out.println(chat);
		System.out.println();

		Chien chien = new Chien("noir cendré", 12);
		chien.boire();
		chien.manger();
		chien.deplacer();
		chien.crier();
		chien.faireLeBeau();
		chien.faireLechouille();
		chien.faireCalin();
		System.out.println(chien);
	}

}
