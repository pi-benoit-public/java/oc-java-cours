package fr.oc.partie2.patternDecorator;

public abstract class Patisserie {

	public abstract String preparer();

}
