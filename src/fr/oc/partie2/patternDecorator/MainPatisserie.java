package fr.oc.partie2.patternDecorator;

public class MainPatisserie {

	public static void main(String[] args) {

		Patisserie pat = new CoucheChocolat(new CoucheBiscuit(new CoucheCaramel(new Gateau())));
		System.out.println(pat.preparer());

	}

}
