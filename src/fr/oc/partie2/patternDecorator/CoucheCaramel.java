package fr.oc.partie2.patternDecorator;

public class CoucheCaramel extends Couche {

	public CoucheCaramel(Patisserie pat) {
		super(pat);
		this.nom = "Une couche de caramel.\n";
	}

}
