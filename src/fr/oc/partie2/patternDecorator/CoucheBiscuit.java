package fr.oc.partie2.patternDecorator;

public class CoucheBiscuit extends Couche {

	public CoucheBiscuit(Patisserie pat) {
		super(pat);
		this.nom = "Une couche de biscuit.\n";
	}

}
