package fr.oc.partie2.patternDecorator;

public class CoucheChocolat extends Couche {

	public CoucheChocolat(Patisserie pat) {
		super(pat);
		this.nom = "Une couche de chocolat.\n";
	}

}
