package fr.oc.partie2.flux;

import java.io.Serializable;

public class Game implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nom, style;
	private double prix;

	// Avec transient, cette variable ne sera pas sérialisée. Elle sera tout bonnement ignorée.
	private transient Notice notice;

	public Game(String nom, String style, double prix) {
		super();
		this.nom = nom;
		this.style = style;
		this.prix = prix;
		this.setNotice(new Notice());
	}

	public String toString() {
		return "Nom du jeu : " + this.nom + " | Style de jeu : " + this.style + " | Prix du jeu : " + this.prix;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public Notice getNotice() {
		return notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

}
