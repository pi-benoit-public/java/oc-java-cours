package fr.oc.partie2.flux;

import java.io.File;

public class MainFile {

	public static void main(String[] args) {

		File f = new File("test.txt");
		System.out.println("Chemin absolu du fichier : " + f.getAbsolutePath());
		System.out.println("Nom du fichier : " + f.getName());
		System.out.println("Est-ce que ce fichier existe ? " + f.exists());
		System.out.println("Est-ce un répertoire ? " + f.isDirectory());
		System.out.println("Est-ce un fichier ? " + f.isFile());

		System.out.println();
		System.out.println("Affichage des fichiers et des dossiers à la racine de cet ordinateur :");
		for (File file : File.listRoots()) {
			System.out.println(file.getAbsolutePath());
			try {
				// On parcourt la liste des fichiers et répertoires.
				for (File nom : file.listFiles()) {
					// S'il s'agit d'un dossier, on ajoute un "/"
					System.out.println(nom.isDirectory() ? nom.getName() + "/" : nom.getName());
				}
			} catch (NullPointerException e) {
				// L'instruction peut générer un NullPointerException s'il n'y a pas de
				// sous-fichier.
			}
		}

	}

}
