package fr.oc.partie2.flux;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

public class MainStringWriterAndReader {

	public static void main(String[] args) {

		StringWriter sw = new StringWriter();
		StringReader sr;

		try {
			sw.write("Coucou les Zéros");
			System.out.println(sw);
			sw.close();
			sr = new StringReader(sw.toString());

			int i;
			// On remet tous les caractères lus dans un String
			String str = "";
			while ((i = sr.read()) != -1)
				str += (char) i;

			System.out.println(str);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
