package fr.oc.partie2.flux;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/*
 * @link https://openclassrooms.com/courses/apprenez-a-programmer-en-java/les-flux-d-entree-sortie#/id/r-22670
 * Pour voir d'autres exemples...
 */
public class MainFileJava7NIOii {

	public static void main(String[] args) {

		File f = new File("test.txt");
		System.out.println("Chemin absolu du fichier : " + f.getAbsolutePath());
		System.out.println("Nom du fichier : " + f.getName());
		System.out.println("Est-ce que ce fichier existe ? " + f.exists());
		System.out.println("Est-ce un répertoire ? " + f.isDirectory());
		System.out.println("Est-ce un fichier ? " + f.isFile());

		/*
		 * On récupère maintenant la liste des répertoires dans une collection typée via
		 * l'objet FileSystem qui représente le système de fichier de l'OS hébergeant la
		 * JVM.
		 */
		Iterable<Path> roots = FileSystems.getDefault().getRootDirectories();

		// Maintenant, il ne nous reste plus qu'à parcourir.
		for (Path chemin : roots) {
			System.out.println(chemin);
			/*
			 * Pour lister un répertoire, il faut utiliser l'objet DirectoryStream. L'objet
			 * Files permet de créer ce type d'objet afin de pouvoir l'utiliser.
			 */
			try (DirectoryStream<Path> listing = Files.newDirectoryStream(chemin)) {

				int i = 0;
				for (Path nom : listing) {
					System.out.print(Files.isDirectory(nom) ? nom + "/" : nom);
					i++;
					System.out.println();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			// Ne prendra en compte que les fichier ayant l'extension .txt
//			try(DirectoryStream<Path> listing = Files.newDirectoryStream(chemin,
			// "*.txt")){ … }

			// Pour copier le fichiertest.txt vers un fichiertest2.txt
//			Path source = Paths.get("test.txt");
//			Path cible = Paths.get("test2.txt");
//			try {
//				Files.copy(source, cible, StandardCopyOption.REPLACE_EXISTING);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

		}

	}

}
