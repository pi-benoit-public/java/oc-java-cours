package fr.oc.partie2.flux;

public class Notice {

	private String langue;

	public Notice() {
		super();
		this.langue = "Français";
	}

	public Notice(String langue) {
		super();
		this.langue = langue;
	}

	public String toString() {
		return "La langue de la notice est " + this.langue;
	}

	public String getLangue() {
		return langue;
	}

	public void setLangue(String langue) {
		this.langue = langue;
	}

}
