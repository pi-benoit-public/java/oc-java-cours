package fr.oc.partie2.exception;

public class MainException {

	public static void main(String[] args) {
		int i = 0, j = 20;

		try {
			System.out.println(j/i);
		} catch(ArithmeticException e) {
			System.out.println("Division par zéro impossible !"+ e.getMessage());
		}

		try {
			System.out.println("test2 = "+ j/i);
		} catch (ClassCastException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Action faite systématiquement.");
		}

	}

}
