package fr.oc.partie2.javaUtilFunction;

import java.util.function.Consumer;
import java.util.function.ToDoubleFunction;

/*
 * Référence de méthodes
 * Syntaxe : « classe, interface ou instance » :: « Nom de la méthode »
 */
public class MainMethodsReference {

	public static void main(String[] args) {

		// Conversion d'un String en Double avec une référence à une méthode statique.
		ToDoubleFunction<String> stringToDoubleLambda = (s) -> Double.parseDouble(s);
		ToDoubleFunction<String> stringToDoubleRef = Double::parseDouble;
		System.out.println(stringToDoubleLambda.applyAsDouble("0.1234567"));
		System.out.println(stringToDoubleRef.applyAsDouble("0.1234567"));

		/*
		 * Utilisation d'une référence à une méthode d'instance (println) de l'instance
		 * out de la classe 'System'.
		 */
		Consumer<String> stringPrinterLambda = (s) -> System.out.println(s);
		Consumer<String> stringPrinterRef = System.out::println;
		stringPrinterLambda.accept("Bonjour Lambda !");
		stringPrinterRef.accept("Bonjour Référence !");

	}

}
