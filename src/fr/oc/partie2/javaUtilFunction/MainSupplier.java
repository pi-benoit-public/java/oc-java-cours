package fr.oc.partie2.javaUtilFunction;

import java.util.function.Supplier;

/*
 * Retourne ce que nous lui demandons de retourner !!!
 */
public class MainSupplier {

	public static void main(String[] args) {

		Supplier<String> s1 = () -> new String("Hello");
		System.out.println(s1.get());

		Supplier<Personne> s2 = () -> new Personne(50, "Dédé");
		System.out.println(s2.get());

	}

}
