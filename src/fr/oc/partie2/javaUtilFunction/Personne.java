package fr.oc.partie2.javaUtilFunction;

public class Personne {

	private int age;
	private String nom;

	public Personne(int age, String nom) {
		super();
		this.age = age;
		this.nom = nom;
	}

	public String toString() {
		return "Nom : " + this.nom + " ; Age : " + this.age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
