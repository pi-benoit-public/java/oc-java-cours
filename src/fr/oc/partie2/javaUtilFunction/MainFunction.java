package fr.oc.partie2.javaUtilFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/*
 * Transformer une collection de Personne en collection de String représentant leurs noms ou en
 * collection d'entier représentant leurs ages.
 */
public class MainFunction {

	public static void main(String[] args) {

		List<Personne> lPersonne = Arrays.asList(new Personne(10, "toto"), new Personne(20, "titi"),
				new Personne(30, "tata"), new Personne(40, "tutu"));

		// Retourne le nom de l'objet Personne.
		Function<Personne, String> f1 = (Personne p) -> p.getNom();
		System.out.println("TEST1 " + transformToListString(lPersonne, f1));

		// Retourne l'âge multiplié par 2 de l'objet Personne.
		Function<Personne, Integer> f2 = (Personne p) -> p.getAge() * 2;
		System.out.println("TEST2 " + transformToListInt(lPersonne, f2));

		System.out.println();

		/*
		 * Autre moyen
		 * Surcharge une méthode par défaut de ces interfaces fonctionnelles.
		 * Avec la méthode  addThen qui permet d'appliquer une fonction après le traitement.
		 */
		Function<Personne, String> fm1 = (Personne p) -> p.getNom();
		// On ne multiplie plus l'âge par 2.
		Function<Personne, Integer> fm2 = (Personne p) -> p.getAge();
		// Traitement supplémentaire sur l'âge.
		Function<Integer, Integer> fm3 = (Integer a) -> a * 2;
		System.out.println("TEST fm1 " + transformToListString(lPersonne, fm1));
		System.out.println("TEST fm2 puis fm3 " + transformToListInt(lPersonne, fm2.andThen(fm3)));

	}

	public static List<String> transformToListString(List<Personne> list, Function<Personne, String> func) {
		List<String> ls = new ArrayList<>();
		for (Personne p : list) {
			ls.add(func.apply(p));
		}
		return ls;
	}

	public static List<Integer> transformToListInt(List<Personne> list, Function<Personne, Integer> func) {
		List<Integer> ls = new ArrayList<>();
		for (Personne p : list) {
			ls.add(func.apply(p));
		}
		return ls;
	}

}
