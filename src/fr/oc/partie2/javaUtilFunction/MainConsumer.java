package fr.oc.partie2.javaUtilFunction;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/*
 * Se contente de "consommer" un objet. Elle ne retourne rien. Elle applique un traitement,
 * Par exemple ici : ajoute 13 ans à l'age d'un objet Personne.
 */
public class MainConsumer {

	public static void main(String[] args) {

		List<Personne> lPersonne = Arrays.asList(new Personne(10, "toto"), new Personne(20, "titi"),
				new Personne(30, "tata"), new Personne(40, "tutu"));

		System.out.println("***Avant***");
		System.out.println(lPersonne);

		Consumer<Personne> c = (Personne p) -> p.setAge(p.getAge() + 13);
		for (Personne p : lPersonne) {
			c.accept(p);
		}

		System.out.println("***Après***");
		System.out.println(lPersonne);

	}

}
