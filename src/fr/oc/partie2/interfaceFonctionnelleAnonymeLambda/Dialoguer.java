package fr.oc.partie2.interfaceFonctionnelleAnonymeLambda;

@FunctionalInterface
public interface Dialoguer {
	public void parler(String message);
}
