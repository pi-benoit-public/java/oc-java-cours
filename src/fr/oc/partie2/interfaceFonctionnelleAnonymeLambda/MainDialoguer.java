package fr.oc.partie2.interfaceFonctionnelleAnonymeLambda;

public class MainDialoguer {

	public static void main(String[] args) {

		// Classe anonyme.
		Dialoguer dial = new Dialoguer() {

			@Override
			public void parler(String message) {
				System.out.println("Tu as dis : " + message);
			}
		};
		dial.parler("Bonjour");

		// Lambda.
		Dialoguer d = (msg) -> System.out.println("Tu as dis : " + msg);
		d.parler("coucou");
	}

}
