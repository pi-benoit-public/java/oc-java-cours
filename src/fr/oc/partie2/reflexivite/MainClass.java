package fr.oc.partie2.reflexivite;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/*
 * Réflexivité : découverte de façon dynamique des informations relatives à une classe ou à un objet.
 */
public class MainClass {

	public static void main(String[] args) {

		// On récupère un objet Class
		Class<String> c = String.class;
		// Ceci fait la même chose.
		Class<? extends String> c2 = new String().getClass();

		System.out.println(c);
		System.out.println(c2);

		System.out.println(
				"La superclasse de la classe " + String.class.getName() + " est : " + String.class.getSuperclass());
		System.out.println(
				"La superclasse de la classe " + Object.class.getName() + " est : " + Object.class.getSuperclass());

		System.out.println("\n*********************************************");

		// La méthode getInterfaces retourne un tableau de Class.
		Class[] faces = c.getInterfaces();
		// Pour voir le nombre d'interfaces.
		System.out.println("Il y a " + faces.length + " interfaces implémentées pour la classe " + c.getName());
		// On parcourt le tableau d'interfaces.
		for (int i = 0; i < faces.length; i++)
			System.out.println("- " + faces[i]);

		System.out.println("\n*********************************************");

		/*
		 * Connaître le nombre de méthodes
		 */
		Method[] m = c.getMethods();

		System.out.println("Il y a " + m.length + " méthodes dans cette classe " + c.getName());
		// On parcourt le tableau de méthodes.
		for (int i = 0; i < m.length; i++) {
			// Affiche la liste des méthodes.
			System.out.println(m[i]);

			// Liste des arguments de ces méthodes.
			Class[] p = m[i].getParameterTypes();
			for (int j = 0; j < p.length; j++)
				System.out.println(p[j].getName());

			System.out.println("----------------------------------\n");

		}

		System.out.println("\n*********************************************");

		/*
		 * Liste des champs de la class String.
		 */
		Field[] mf = c.getDeclaredFields();

		System.out.println("Il y a " + mf.length + " champs dans cette classe " + c.getName());
		// On parcourt le tableau de méthodes
		for (int i = 0; i < mf.length; i++)
			System.out.println("- " + mf[i].getName());

		System.out.println("\n*********************************************");

		/*
		 * Liste les constructeurs de cette classe.
		 */
		Constructor[] construc = c.getConstructors();
		System.out.println("Il y a " + construc.length + " constructeurs dans cette classe " + c.getName());
		// On parcourt le tableau des constructeurs.
		for (int i = 0; i < construc.length; i++) {
			System.out.println(construc[i].getName());

			Class[] param = construc[i].getParameterTypes();
			for (int j = 0; j < param.length; j++)
				System.out.println(param[j]);

			System.out.println("-----------------------------\n");
		}

	}

}
