package fr.oc.partie2.ville;

public class Ville {

	public static int nbInstances = 0;

	protected static int nbInstancesBis = 0;

	protected String nomVille;
	protected String nomPays;
	protected int nbHabitants;
	protected char categorie;

	public Ville() {
		System.out.println("Création d'une ville");
		this.nomVille = "inconnu";
		this.nomPays = "inconnu";
		this.nbHabitants = 0;
		this.setCategorie();
		nbInstances++;
		nbInstancesBis++;
	}

	public Ville(String ville, String pays, int nombreHabitants) throws NombreHabitantsException {
		if (nombreHabitants < 0) {
			throw new NombreHabitantsException(nombreHabitants);
		} else {
			System.out.println("Création d'une ville avec paramètres");
			this.nomVille = ville;
			this.nomPays = pays;
			this.nbHabitants = nombreHabitants;
			this.setCategorie();
			nbInstances++;
			nbInstancesBis++;
		}
	}

	/**
	 * Retourne la description d'une ville.
	 * 
	 * @return String
	 */
	public String description() {
		return this.nomVille + " est une ville de " + this.nomPays + " qui a " + this.nbHabitants
				+ " habitants. C'est une ville de catégorie " + this.categorie + ".";
	}

	/**
	 * Compare le nombre d'habitants de deux villes.
	 * 
	 * @param v
	 *            String
	 * @return String
	 */
	public String comparer(Ville v) {
		String str;
		if (v.getNbHabitants() > this.nbHabitants)
			str = v.getNomVille() + " est une ville plus peuplée que " + this.nomVille;
		else
			str = v.getNomVille() + " est une ville moins peuplée que " + this.nomVille;
		return str + ".";
	}

	public String getNomVille() {
		return nomVille;
	}

	public void setNomVille(String nomVille) {
		this.nomVille = nomVille;
	}

	public String getNomPays() {
		return nomPays;
	}

	public void setNomPays(String nomPays) {
		this.nomPays = nomPays;
	}

	public int getNbHabitants() {
		return nbHabitants;
	}

	public void setNbHabitants(int nbHabitants) {
		this.nbHabitants = nbHabitants;
	}

	public char getCategorie() {
		return categorie;
	}

	public void setCategorie() {
		int bornesSuperieures[] = { 0, 1000, 10000, 100000, 500000, 1000000, 5000000, 10000000 };
		char categories[] = { '?', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
		int i = 0;

		while (i < bornesSuperieures.length && this.nbHabitants > bornesSuperieures[i])
			i++;

		this.categorie = categories[i];
	}

	public static int getNbInstancesBis() {
		return nbInstancesBis;
	}

}
