package fr.oc.partie2.ville;

public class MainVille {

	public static void main(String[] args) {

		Ville ville = new Ville();
		Ville grenoble = null;
		Ville nyc = null;
		Ville noman = null;

		try {
			grenoble = new Ville("Grenoble", "France", 160_649);
			nyc = new Ville("New York City", "Etats-Unis", 8_537_673);
			noman = new Ville("no man", "ailleurs", -100);
		} catch (NombreHabitantsException e) {
			
		} finally {
			noman = ville;
		}

		System.out.println(ville.description());
		System.out.println(grenoble.description());
		System.out.println(nyc.description());
		System.out.println(noman.description());
		System.out.println("Le nombre d'instances publiques sont "+ Ville.nbInstances);
		System.out.println("Le nombre d'instances privées sont "+ Ville.getNbInstancesBis());

		
		Capitale cap = new Capitale();
		Capitale paris = null;

		try {
			paris = new Capitale("Paris", "France", 2_206_488, "Tour Eiffel");
		} catch (NombreHabitantsException e) {}

		System.out.println(cap.description());
		System.out.println(paris.description());
		System.out.println(grenoble.comparer(paris));
		System.out.println(paris.comparer(nyc));
		System.out.println("Le nombre d'instances publiques sont "+ Ville.nbInstances);
		System.out.println("Le nombre d'instances privées sont "+ Ville.getNbInstancesBis());

	}

}
