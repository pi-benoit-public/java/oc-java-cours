package fr.oc.partie2.ville;

public class NombreHabitantsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NombreHabitantsException() {
		System.out.println("Vous essayez d'instancier une classe Ville avec un nombre d'habitants négatif !");
	}

	public NombreHabitantsException(int nbr) {
		System.out.println("Instanciation avec un nombre d'habitants négatif.");
		System.out.println("\t => " + nbr);
	}

}
