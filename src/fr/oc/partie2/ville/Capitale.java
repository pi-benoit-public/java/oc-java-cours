package fr.oc.partie2.ville;

public class Capitale extends Ville {

	private String monument;

	public Capitale() {
		super();
		this.monument = "aucun";
	}

	public Capitale(String ville, String pays, int nombreHabitants, String monument) throws NombreHabitantsException {
		super(ville, pays, nombreHabitants);
		this.monument = monument;
	}

	public String description() {
		String str = super.description();
		str += " Monument : "+ monument +".\n";
		return str;
	}

	public String getMonument() {
		return monument;
	}

	public void setMonument(String monument) {
		this.monument = monument;
	}

}
