package fr.oc.partie2.collection;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

public class MainMap {

	public static void main(String[] args) {

		/*
		 * Objet HashTable (table de hachage).
		 * 
		 * N'accepte pas les valeurs null.
		 * Est Thread Safe, c'est-à-dire qu'il est utilisable dans plusieurs threads
		 * (cela signifie que plusieurs éléments de votre programme peuvent l'utiliser simultanément.
		 */
		Hashtable<Object, Object> ht = new Hashtable<>();
		// Les clés ne sont pas obligés de se suivre de 1 à 1.
		ht.put(1, "Printemps");
		ht.put(10, "Ete");
		ht.put(15, "Automne");
		ht.put(33, "Hiver");

		Enumeration<Object> e = ht.elements();
		while (e.hasMoreElements()) {
			// L'affichage se fait comme il veut ?!?
			System.out.println(e.nextElement());
		}

		/*
		 * HashMap
		 * 
		 * Similaire à HashTable à la différence :
		 * Accepte des valeurs null.
		 * N'est pas Thread Safe.
		 */

		/*
		 * Set
		 * 
		 * N'accepte pas les doublons.
		 * 
		 * Les Set sont particulièrement adaptés pour manipuler une grande quantité de données.
		 * Cependant, les performances de ceux-ci peuvent être amoindries en insertion.
		 * Généralement, on opte pour un HashSet, car il est plus performant en temps d'accès,
		 * mais si vous avez besoin que votre collection soit constamment triée, optez pour un TreeSet.
		 */

		/*
		 * Object HashSet
		 */
		HashSet<Serializable> hs = new HashSet<Serializable>();
		hs.add("toto");
		hs.add(12);
		hs.add('d');

		System.out.println("\nParcours avec Iterator");
		System.out.println("----------------------");
		Iterator<Serializable> it = hs.iterator();
		while(it.hasNext())
			System.out.println(it.next());

		System.out.println("\nParcours avec un tableau d'objet");
		System.out.println("--------------------------------");

		Object[] obj = hs.toArray();
		for(Object o : obj)
			System.out.println(o);

	}

}
