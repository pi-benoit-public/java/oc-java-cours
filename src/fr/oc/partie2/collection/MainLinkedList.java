package fr.oc.partie2.collection;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/*
 * Si vous insérez beaucoup de données au milieu de la liste, optez pour une Linkedlist.
 */
public class MainLinkedList {

	public static void main(String[] args) {

		List<Serializable> ll = new LinkedList<Serializable>();
		ll.add(0);
		ll.add("une chaîne");
		ll.add(12.5f);

		System.out.println("## Premier for");
		for (int i=0; i < ll.size(); i++) {
			System.out.println("Element index i = "+ i + " est "+ ll.get(i));
		}

		System.out.println();

		System.out.println("Parcours avec un itérateur");
		System.out.println("--------------------------");
		ListIterator<Serializable> li = ll.listIterator();
		while (li.hasNext())
			System.out.println(li.next());

	}

}
