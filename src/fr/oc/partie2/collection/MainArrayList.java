package fr.oc.partie2.collection;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Si vous effectuez beaucoup de lectures sans vous soucier de l'ordre des éléments, optez pour une ArrayList.
 */
public class MainArrayList {

	public static void main(String[] args) {

		ArrayList<Serializable> al = new ArrayList<Serializable>();
		al.add(0);
		al.add("une chaîne");
		al.add(12.5f);

		System.out.println("## Premier for");
		for (int i=0; i < al.size(); i++) {
			System.out.println("Element index i = "+ i + " est "+ al.get(i));
		}

	}

}
