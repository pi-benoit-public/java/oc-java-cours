package fr.oc.partie2.patternStrategy;

import fr.oc.partie2.patternStrategy.comportement.*;

public class Sniper extends Personnage {

	public Sniper() {
		super();
		this.espritCombatif = new CombatFusilALunette();
	}

	public Sniper(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
		super(espritCombatif, soin, deplacement);
	}

}
