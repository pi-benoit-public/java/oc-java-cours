package fr.oc.partie2.patternStrategy;

import fr.oc.partie2.patternStrategy.comportement.*;

public abstract class Personnage {

	// Instances de comportement
	protected EspritCombatif espritCombatif = new Pacifiste();
	protected Soin soin = new AucunSoin();
	protected Deplacement deplacement = new Marcher();

	// Constructeur par défaut.
	public Personnage() {};

	// Constructeur avec paramètres.
	public Personnage(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
		super();
		this.espritCombatif = espritCombatif;
		this.soin = soin;
		this.deplacement = deplacement;
	}

	//Méthode de déplacement de personnage.
	public void seDeplacer() {
		// On utilise les objets de déplacement de façon polymorphe.
		deplacement.deplace();
	};

	//Méthode que les combattants utilisent.
	public void combattre() {
		espritCombatif.combat();
	};

	public void soigner() {
		soin.soigne();
	}

	public void setEspritCombatif(EspritCombatif espritCombatif) {
		this.espritCombatif = espritCombatif;
	}

	public void setSoin(Soin soin) {
		this.soin = soin;
	}

	public void setDeplacement(Deplacement deplacement) {
		this.deplacement = deplacement;
	}

}
