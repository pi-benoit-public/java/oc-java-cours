package fr.oc.partie2.patternStrategy;

public class Main {

	public static void main(String[] args) {
		Personnage[] tPers = {
				new Chirurgien(),
				new Civil(),
				new Guerrier(),
				new Medecin(),
				new Sniper()
		};

		for (int i = 0; i < tPers.length; i++) {
			System.out.println("\nInstance de "+ tPers[i].getClass().getName());
			System.out.println("***************************************");
			tPers[i].combattre();
			tPers[i].seDeplacer();
			tPers[i].soigner();
		}


	}

}
