package fr.oc.partie2.patternStrategy;

import fr.oc.partie2.patternStrategy.comportement.*;

public class Guerrier extends Personnage {

	public Guerrier() {
		super();
		this.espritCombatif = new CombatPistolet();
	}

	public Guerrier(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
		super(espritCombatif, soin, deplacement);
	}

}
