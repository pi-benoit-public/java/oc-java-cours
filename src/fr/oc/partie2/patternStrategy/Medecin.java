package fr.oc.partie2.patternStrategy;

import fr.oc.partie2.patternStrategy.comportement.*;

public class Medecin extends Personnage {

	public Medecin() {
		super();
		this.soin = new PremierSoin();
	}

	public Medecin(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
		super(espritCombatif, soin, deplacement);
	}

}
