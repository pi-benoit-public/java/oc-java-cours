package fr.oc.partie2.patternStrategy;

import fr.oc.partie2.patternStrategy.comportement.*;

public class Chirurgien extends Personnage {

	public Chirurgien() {
		super();
		this.soin = new OperationSoin();
	}

	public Chirurgien(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
		super(espritCombatif, soin, deplacement);
	}

}
