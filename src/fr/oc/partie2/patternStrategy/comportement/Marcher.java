package fr.oc.partie2.patternStrategy.comportement;

public class Marcher implements Deplacement {

	@Override
	public void deplace() {
		System.out.println("Je me déplace en marchant.");
	}

}
