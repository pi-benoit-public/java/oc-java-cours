package fr.oc.partie2.patternStrategy.comportement;

public class CombatCouteau implements EspritCombatif {

	@Override
	public void combat() {
		System.out.println("Je combats au couteau.");
	}

}
