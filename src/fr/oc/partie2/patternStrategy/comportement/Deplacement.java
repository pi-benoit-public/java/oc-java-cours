package fr.oc.partie2.patternStrategy.comportement;

public interface Deplacement {

	public abstract void deplace();

}
