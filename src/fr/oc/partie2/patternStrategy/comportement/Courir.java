package fr.oc.partie2.patternStrategy.comportement;

public class Courir implements Deplacement {

	@Override
	public void deplace() {
		System.out.println("Je cours !");
	}

}
