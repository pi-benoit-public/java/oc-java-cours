package fr.oc.partie2.patternStrategy.comportement;

public interface EspritCombatif {

	public abstract void combat();

}
