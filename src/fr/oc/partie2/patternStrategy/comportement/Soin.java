package fr.oc.partie2.patternStrategy.comportement;

public interface Soin {

	public abstract void soigne();

}
