package fr.oc.partie2.patternStrategy.comportement;

public class OperationSoin implements Soin {

	@Override
	public void soigne() {
		System.out.println("Je fais des opérations chirurgicales.");
	}

}
