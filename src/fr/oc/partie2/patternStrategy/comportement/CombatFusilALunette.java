package fr.oc.partie2.patternStrategy.comportement;

public class CombatFusilALunette implements EspritCombatif {

	@Override
	public void combat() {
		System.out.println("Je tire sur mes cibles grâce à mon fusil à lunettes.");
	}

}
