package fr.oc.partie2.patternStrategy.comportement;

public class CombatPistolet implements EspritCombatif {

	@Override
	public void combat() {
		System.out.println("Je combat au pistolet.");
	}

}
