package fr.oc.partie2.patternStrategy.comportement;

public class Operation implements Soin {

	@Override
	public void soigne() {
		System.out.println("Je réalise des opérations.");
	}

}
