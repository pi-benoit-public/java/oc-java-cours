package fr.oc.partie2.patternStrategy.comportement;

public class AucunSoin implements Soin {

	@Override
	public void soigne() {
		System.out.println("Je ne peux pas soigner.");
	}

}
