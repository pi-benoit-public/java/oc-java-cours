package fr.oc.partie2.patternStrategy;

import fr.oc.partie2.patternStrategy.comportement.*;

public class Civil extends Personnage {

	public Civil() {
		super();
	}

	public Civil(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
		super(espritCombatif, soin, deplacement);
	}

}
