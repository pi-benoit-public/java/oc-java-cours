package fr.oc.partie2.genericite;

import java.util.ArrayList;
import java.util.List;

public class MainDuo {

	public static void main(String[] args) {

		Duo<String, Boolean> dual = new Duo<String, Boolean>("Ma string", true);
		System.out.println("Valeur de l'objet dual : val1 = " + dual.getValeurT() + ", val2 = " + dual.getValeurT2());

		Duo<Double, Character> dual2 = new Duo<Double, Character>(18.5d, 'a');
		System.out.println("Valeur de l'objet dual : val1 = " + dual2.getValeurT() + ", val2 = " + dual2.getValeurT2());

		/*
		 * Généricité sur les collections.
		 */
		System.out.println("\nListe de String");
		System.out.println("-----------------");
		List<String> listeString= new ArrayList<String>();
		listeString.add("Une chaîne");
		listeString.add("Une autre");
		listeString.add("Encore une autre");
		listeString.add("Allez, une dernière");

		for(String str : listeString)
			System.out.println(str);

		System.out.println("\nListe de float");
		System.out.println("--------------");
		List<Float> listeFloat = new ArrayList<Float>();
		listeFloat.add(12.25f);
		listeFloat.add(15.25f);
		listeFloat.add(2.25f);
		listeFloat.add(128764.25f);

		for(float f : listeFloat)
			System.out.println(f);

	}

}
