package fr.oc.partie2.genericite;

public class Solo<T> {

	private T valeur;

	public Solo() {
		super();
	}

	public Solo(T valeur) {
		super();
		this.valeur = valeur;
	}

	public T getValeur() {
		return valeur;
	}

	public void setValeur(T valeur) {
		this.valeur = valeur;
	}

}
