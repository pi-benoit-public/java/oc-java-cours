package fr.oc.partie2.genericite;

public class MainSolo {

	public static void main(String[] args) {

		Solo<Integer> valInt = new Solo<Integer>(12);
		Integer nb = valInt.getValeur();
		System.out.println(nb);

		Solo<String> valString = new Solo<String>("Je suis de type string.");
		Solo<Float> valFloat = new Solo<Float>(84.2f);
		Solo<Double> valDouble = new Solo<Double>(43.202568);

		System.out.println(valString.getValeur());
		System.out.println(valFloat.getValeur());
		System.out.println(valDouble.getValeur());

	}

}
