package fr.oc.partie2.genericite;

public class Duo<T,T2> {

	private T valeurT;
	private T2 valeurT2;

	public Duo() {
		super();
		this.valeurT = null;
		this.valeurT2 = null;
	}

	public Duo(T valeurT, T2 valeurT2) {
		super();
		this.valeurT = valeurT;
		this.valeurT2 = valeurT2;
	}

	// Définit les deux valeurs.
	public void setValeur(T valeurT, T2 valeurT2) {
		this.valeurT = valeurT;
		this.valeurT2 = valeurT2;
	}

	public T getValeurT() {
		return valeurT;
	}

	public void setValeurT(T valeurT) {
		this.valeurT = valeurT;
	}

	public T2 getValeurT2() {
		return valeurT2;
	}

	public void setValeurT2(T2 valeurT2) {
		this.valeurT2 = valeurT2;
	}

}
