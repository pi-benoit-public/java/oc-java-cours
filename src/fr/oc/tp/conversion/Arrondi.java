package fr.oc.tp.conversion;

/**
 * Arrondi
 * @author pi-benoit
 *
 */
public class Arrondi {

	/**
	 * Arrondi un résultat après la virgule.
	 * @param A double Le nombre à arrondir.
	 * @param B int Le nombre de chiffres après la virgule.
	 * @return double
	 */
	public static double arrondi(double A, int B) {
		return (double) ( (int) (A * Math.pow(10, B) + .5)) / Math.pow(10, B);
	}

}
