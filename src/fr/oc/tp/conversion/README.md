# TP : conversion Celsius - Fahrenheit

## Langage

Java

## Description

Voici les caractéristiques du programme :

- le programme demande quelle conversion nous souhaitons effectuer,
Celsius vers Fahrenheit ou l'inverse

- on n'autorise que les modes de conversion définis dans le programme
(un simple contrôle sur la saisie fera l'affaire)

- on demande à la fin à l'utilisateur s'il veut faire une nouvelle
conversion, ce qui signifie que l'on doit pouvoir revenir au début du
programme.

Formule de conversion pour passer des degrés Celsius en degrés Fahrenheit :

F = (9 / 5) * C + 32

Formule de conversion pour passer des degrés Fahrenheit en degrés Celsius :

C = ( (F - 32 ) * 5 ) / 9
