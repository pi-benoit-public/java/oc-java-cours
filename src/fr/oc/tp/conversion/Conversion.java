package fr.oc.tp.conversion;

import java.util.Scanner;

/**
 * Convertisseur degrés Celsius <-> Fahrenheit.
 * @author pi-benoit
 *
 */
public class Conversion {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		char reponseChoix = ' ';
		char reponseContinue = ' ';
		double temperature;
		double conversion;

		String titre = "CONVERTISSEUR DEGRES CELSIUS <-> FAHRENHEIT";
		String msgConversion = "Choisissez le mode de conversion\n";
		msgConversion += "1 - Convertisseur Celsius en Fahrenheit\n";
		msgConversion += "2 - Convertisseur Fahrenheit en Celsius";

		System.out.println(titre);

		do { // reponseContinue != 'N'

			do { // reponseChoix == '1' && reponseChoix == '2'

				// 1er message de conversion.
				System.out.println(msgConversion);
				reponseChoix = sc.nextLine().charAt(0);

				// Obligation d'entrer 1 ou 2 sinon la boucle suivante est exécutée.
				while (reponseChoix != '1' && reponseChoix != '2') {
					reponseChoix = ' ';
					System.out.println("Choisir 1 ou 2");
					reponseChoix = sc.nextLine().charAt(0);
				}

			} while (reponseChoix != '1' && reponseChoix != '2');

			// Le choix de conversion a bien été entré, on peut convertir et afficher le résultat.
			if (reponseChoix == '1') {
				System.out.println("Température Celsius à convertir en Farhenheit");
				temperature = sc.nextDouble();
				sc.nextLine();
				conversion = (9.0d / 5.0d) * temperature + 32.0d;
				temperature = Arrondi.arrondi(temperature, 2);
				conversion = Arrondi.arrondi(conversion, 2);
				System.out.println(temperature +"°C correspond à "+ conversion +"°F");
			} else {
				System.out.println("Température Farenheit à convertir en Celsius");
				temperature = sc.nextDouble();
				sc.nextLine();
				conversion = ((temperature - 32.0d) * 5.0d) / 9.0d;
				temperature = Arrondi.arrondi(temperature, 2);
				conversion = Arrondi.arrondi(conversion, 2);
				System.out.println(temperature + "°F correspond à " + conversion + "°C");
			}

			// On demande si l'on veut une autre conversion.
			System.out.println("Voulez-vous continuer ? (O/N)");
			reponseContinue = sc.nextLine().charAt(0);

			// On veut absolument O ou N sinon on redemande avec cette boucle.
			while (reponseContinue != 'O' && reponseContinue != 'N') {
				reponseContinue = ' ';
				System.out.println("O pour oui - N pour non");
				reponseContinue = sc.nextLine().charAt(0);
			}

		} while (reponseContinue != 'N');

		System.out.println("Au revoir.");

		sc.close();

	}

}
