package fr.oc.partie1;

public class For {

	public static void main(String[] args) {

		int i, j;

		// Parcours ascendant.
		System.out.println("Parcours ascendant");
		for (i = 1; i <= 5; i++) {
			System.out.println("Ligne "+ i);
		}

		// Parcours descendant.
		System.out.println("Parcours descendant");
		for (i = 5; i > 0; i--) {
			System.out.println("Ligne "+ i);
		}

		// Parcours avec deux conditions.
		System.out.println("Parcours avec deux conditions");
		for(i = 1, j = 2; (i < 10 && j < 6); i++, j += 2) {
			System.out.println("i = " + i + ", j = " + j);
		}

	}

}
