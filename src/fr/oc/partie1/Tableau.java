package fr.oc.partie1;

public class Tableau {

	/**
	 * Parcours un tableau de type chaînes de caractères.
	 * Affiche chaque case du tableau.
	 * @param tab String
	 */
	public static void parcourirTableau(String tab[]) {
		for (String value : tab) {
			System.out.println(value);
		}
	}

	/**
	 * Parcours un tableau de type entier.
	 * Affiche chaque case du tableau.
	 * @param tab int
	 */
	public static void parcourirTableau(int tab[]) {
		for (int value : tab) {
			System.out.println(value);
		}
	}

	/**
	 * Parcours un tableau à deux dimensions de type chaîne de caractères.
	 * Affiche chaque case du tableau.
	 * @param tab String
	 */
	public static void parcourirTableau(String tab[][]) {
		for (String dimension1[] : tab) {
			for(String dimension2 : dimension1) {
				System.out.println(dimension2);
			}
		}
	}

	/**
	 * Retourne le parcours d'un tableau de type chaînes de caractères.
	 * @param tab String
	 * @return String
	 */
	public static String retourneParcourirTableau(String tab[]) {
		String result = "";
		for (String value : tab) {
			result += value +"\n";
		}
		return result;
	}

}
