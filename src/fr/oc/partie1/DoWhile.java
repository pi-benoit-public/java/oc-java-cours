package fr.oc.partie1;

import java.util.Scanner;

public class DoWhile {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String prenom;
		// Pas besoin d'initialiser, on entre au moins une fois dans la boucle.
		char reponse = ' ';

		do {

			System.out.println("Donnez un prénom");
			prenom = sc.nextLine();
			System.out.println("Bonjour "+ prenom +" !");

			do {
				System.out.println("Voulez-vous continuer ? (O/N)");
				reponse = sc.nextLine().charAt(0);
			} while (reponse != 'O' && reponse != 'N');

		} while (reponse == 'O');

		System.out.println("Au revoir.");

		sc.close();

	}

}
