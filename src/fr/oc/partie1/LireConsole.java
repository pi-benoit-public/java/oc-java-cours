package fr.oc.partie1;

// Ceci importe la classe Scanner du package java.util
import java.util.Scanner;

public class LireConsole {

	public static void main(String[] args) {

		/*
		 * Récupérer des données string (une chaîne de caractères) entrées par l'utilisateur
		 * dans la console.
		 */
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez entrer un mot");
		String str = sc.nextLine();
		System.out.println("Vous avez saisi : "+ str);

		// int i = sc.nextInt(); // Pour un entier.
		// double d = sc.nextDouble(); // Pour un double.
		// long l = sc.nextLong(); // Pour un long.
		// byte b = sc.nextByte(); // Pour un byte.

		/*
		 * Important ! On vide la ligne avant d'en lire une autre.
		 * Faire entrer après l'affichage du mot dans la console.
		 */
		sc.nextLine();

		/*
		 * Pour récupérer un caractère avec la classe Scanner.
		 */
		System.out.println("Saisissez une lettre");
		str = sc.nextLine();
		char car = str.charAt(0);
		System.out.println("Vous avez saisi le caractère : "+ car);

		// Pour fermer la connection de l'objet sc.
		sc.close();

	}

}
