package fr.oc.partie1;

import java.util.Scanner;

public class While {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String prenom;
		char reponse = 'O';

		while (reponse == 'O') {
			System.out.println("Donnez un prénom");
			prenom = sc.nextLine();
			System.out.println("Bonjour "+ prenom +" !");

			// Pour rentrer dans la boucle suivante et forcer une réponse à O ou N.
			reponse = ' ';
			while (reponse != 'O' && reponse != 'N') {
				System.out.println("Voulez-vous continuer ? (O/N)");
				reponse = sc.nextLine().charAt(0);
			}
		}

		System.out.println("Au revoir.");

		sc.close();

	}

}
