package fr.oc.partie1;

public class Cast {

	public static void main(String[] args) {

		System.out.println("Hello world!");

		/*
		 * Conversion ou cast.
		 */
		double nbre1 = 10, nbre2 = 3;
		int resultat = (int)(nbre1 / nbre2);
		System.out.println("Le résultat est = " + resultat); // 3

		int nbr1 = 3, nbr2 = 2;
		double resultat2 = nbr1 / nbr2;
		System.out.println("Le résultat2 est = " + resultat2); // 1.0

		int nb1 = 3, nb2 = 2;
		double resultat3 = (double)(nb1 / nb2);
		System.out.println("Le résultat3 est = " + resultat3); // 1.0

		int n1 = 3, n2 = 2;
		double resultat4 = (double)(n1) / (double)(n2);
		System.out.println("Le résultat4 est = " + resultat4); // 1.5

		/*
		 * Conversion d'un int en string.
		 */
		int i = 12;
		String j;
		j = String.valueOf(i);
		System.out.println("Ca ne se voit pas mais c'est un string : "+ j);

		/*
		 * Conversion d'un string en int
		 */
		int k = Integer.valueOf(j).intValue();
		System.out.println("Ca ne se voit pas mais c'est un int : "+ k);

	}

}
