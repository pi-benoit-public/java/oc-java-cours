package fr.oc.partie1;

public class MainTableau {

	public static void main(String[] args) {

		String[] tabStr = {"toto", "titi", "tata"};
		int[] tabInt = {1, 2, 3, 4};
		String[][] tabStr2 = {{"1", "2", "3", "4"}, {"toto", "titi", "tata"}};

		System.out.println("Parcours d'un tableau String");
		Tableau.parcourirTableau(tabStr);
		System.out.println();

		System.out.println("Parcours d'un tableau Int");
		Tableau.parcourirTableau(tabInt);
		System.out.println();

		System.out.println("Parcours d'un tableau à deux dimensions");
		Tableau.parcourirTableau(tabStr2);
		System.out.println();

		System.out.println("Retourne le parcours d'un tableau String");
		System.out.println( Tableau.retourneParcourirTableau(tabStr) );

	}

}
