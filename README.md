# Apprenez à programmer en Java

Suivi du cours [Openclassrooms] (https://openclassrooms.com/courses/26832/next-page-to-do "Apprenez à programmer en Java")


## fr.oc.partie1

- Cast
- DoWhile
- For
- LireConsole
- MainTableau
- Tableau
- While


## fr.oc.partie2.animal


## fr.oc.partie2.collection

- ArrayList
- LinkedList
- Map


## fr.oc.partie2.exception


## fr.oc.partie2.flux

- CharArrayWriter / CharArrayReader
- Compare Buffer io / nio
- File
- FileInput / FileOutput
- File Java 7 nioII
- FileReader / FileWriter
- Lecture Ecriture Type Primitif
- StringWriter / StringReader
- Test exécution lecture
- Test exécution écriture
- Try-with-resources
- Game/Notice Serializable/transient


## fr.oc.partie2.genericite

- Solo
- Duo


## fr.oc.partie2.interfaceFonctionnelleAnonymeLambda

- Interface fonctionnelle
- Classe anonyme
- Lambda


## fr.oc.partie2.javaUtilFunction

- Consumer
- Function
- MethodsReference
- Predicate
- Supplier


## fr.oc.partie2.patternDecorator

- Patisserie/Gâteau/Couche


## fr.oc.partie2.patternStrategy

- Personnage/Civil/Guerrier/Médecin...


## fr.oc.partie2.reflexivite

Réflexivité : découverte de façon dynamique des informations relatives à une classe ou à un objet.


## fr.oc.partie2.ville


## fr.oc.tp.conversion

- Arrondi
- Conversion Celsius <-> Fahrenheit